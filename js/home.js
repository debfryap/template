requirejs.config({
	"baseUrl": "js/vendor",
	"paths": {
		"app": "../home",     
		"jquery" : "jquery-1.9.1.min",
		"modernizr" : "//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min",
		"fancybox" : "//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack",
		"TweenMax" : "//cdnjs.cloudflare.com/ajax/libs/gsap/1.15.1/TweenMax.min",
	},
	//dependencies
	"shim": {
		"fancybox": ["jquery"],        
	}
});

// Load the main app module to start the app
require(['main','fancybox'],function(){
	// code here
	$(".fancybox").fancybox();
});

